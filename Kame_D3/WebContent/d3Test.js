init();
scatterRepresentation();
barRepresentation();

function init() {

	w = 600;
	h = 300;

	transitionSpeed = 3000; // How quickly a transition occurs
	delay = 25; // The delay time between transitions of each data point
	dataset = [];
	var yRange = Math.random() * 1000;
	var numpoints = 100;
	// Populates the dataset
	for (var i = 0 ; i < numpoints ; i++) {
		var newNumber = i;
		var newNumber2 = Math.round(Math.random() * yRange);
		dataset.push([ newNumber, newNumber2 ]);
	}
}

function scatterRepresentation() {

	var pointSize = 3;

	// The padding to keep graphics away from the edge of the SVG
	var padding = 20;
	// Scale along the x axis
	var xScale = getXScale(dataset, [ padding, w - padding * 2 ]);
	// Scale along the y axis
	var yScale = getYScale(dataset, [ h - padding, padding ]);
	// Scale the radii of each point
	var rScale = getYScale(dataset, [ 2, 5 ]);

	// Create SVG element
	var svg = d3.select("body").append("svg");
	svg.attr("width", w).attr("height", h);
	svg.attr("anchor", "middle");

	// The axes of the plot
	var xAxis = d3.svg.axis();
	xAxis.scale(xScale).orient("bottom").ticks(5);

	// Generates the x axis of the scatterplot
	var gx = svg.append("g");
	gx.attr("class", "axis");
	gx.attr("transform", "translate(0," + (h - padding) + ")");
	gx.call(xAxis);

	// The y axis of the scatterplot
	var yAxis = d3.svg.axis().scale(yScale).orient("left").ticks(5);
	var gy = svg.append("g");
	gy.attr("class", "axis");
	gy.attr("transform", "translate(" + padding + ",0)");
	gy.call(yAxis);

	// Creates the circles and positions them at the origin
	var circles =
			svg.selectAll("circle").data(dataset).enter().append("circle");
	circles.attr("cx", function(d) {
		return xScale(d[0]);
	}).attr("cy", h - padding);
	circles.attr("r", pointSize);

	// Does the movement transitions of the circles
	circles.transition().delay(function(d, i) {
		return i * delay;
	}).duration(transitionSpeed / 3).attr("cy", function(d) {
		return yScale(d[1]);
	});
	circles.attr("fill", "black");
	addLineToScatterplot(svg, dataset, xScale, yScale);
}

/**
 * This function draws a line between points in the style defined by the line
 * class
 * 
 * @param svg
 * @param dataToFit
 * @param xscale
 * @param yscale
 */
function addLineToScatterplot(svg, dataToFit, xscale, yscale) {
	var line = d3.svg.line();
	line.x(function(d) {
		return xscale(d[0]);
	}).y(function(d) {
		return yscale(d[1]);
	});
	svg.append("path").attr("class", "line").attr("d", line(dataToFit));

}

/**
 * This function returns a horizontal scale for a given dataset.
 * 
 * @param datasetToScale
 *            the data to scale
 * @param range
 *            usually an svg width and padding value
 * @returns the x scaling function
 */
function getXScale(datasetToScale, range) {

	var newScale = d3.scale.linear();
	newScale.domain([ 0, d3.max(datasetToScale, function(d) {
		return d[0];
	}) ]);
	newScale.range(range);
	return newScale;
}
/**
 * This function returns a vertical scale for a given dataset.
 * 
 * @param datasetToScale
 *            the data to scale
 * @param range
 *            the range of values, usually an svg height and a padding are sent
 * @returns the y scale function
 */
function getYScale(datasetToScale, range) {
	var newScale = d3.scale.linear();
	newScale.domain([ 0, d3.max(datasetToScale, function(d) {
		return d[1];
	}) ]);
	newScale.range(range);
	return newScale;
}

function getLinearScale(datasetToScale, range) {
	var newScale = d3.scale.linear();
	newScale.domain([ 0, d3.max(datasetToScale, function(d) {
		return d;
	}) ]);
	newScale.range(range);
	return newScale;
}

function barRepresentation() {
	var barPadding = 1;
	// var yScale = getYScale(dataset, [ h - padding, padding ]);

	var svg =
			d3.select("body").append("svg").attr("width", w).attr("height", h);
	var rects = svg.selectAll("rect").data(dataset).enter().append("rect");

	var yScale = getYScale(dataset, [ 0, h ]);

	rects.attr("x", function(d, i) {
		return i * (w / dataset.length);
	});
	rects.attr("width", w / dataset.length - barPadding);
	rects.attr("y", h);
	rects.transition().delay(function(d, i) {
		return i * delay;
	}).duration(transitionSpeed).attr("y", function(d) {
		return h - (yScale(d[1]));
	});

	rects.attr("height", function(d) {
		return d[1] * 4;
	});
	rects.attr("fill", "darkblue");
}